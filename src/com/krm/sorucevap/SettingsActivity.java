package com.krm.sorucevap;

import com.krm.sorucevap.util.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity {

	private String token;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		token=getIntent().getStringExtra("TOKEN");
		
		
		Button btDersEkle=(Button) findViewById(R.id.btDersEkle);
	    final EditText etDersAdi=(EditText) findViewById(R.id.etDersAdi);
		
		
		btDersEkle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Utils.saveCategory(etDersAdi.getText().toString(), token);
				etDersAdi.setText("");
				Toast.makeText(getApplicationContext(), "Yeni ders ekledi.", Toast.LENGTH_SHORT).show();
			}
		});
		
	}

	

}
