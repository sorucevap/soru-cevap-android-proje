package com.krm.sorucevap;

import java.util.ArrayList;
import java.util.List;

import com.krm.sorucevap.util.AnswerVO;
import com.krm.sorucevap.util.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CevaplarActivity extends Activity {
	
	private ListView ltCevaplar;
	private List<String> ltCevap;
	private TextView tvCevap;
	
	private String soru;
	private int idquestion;
	private String sorankisi;
	private String date;
	private String token;
	
	List<AnswerVO> answers=new ArrayList<AnswerVO>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cevaplar);
		
		soru=getIntent().getStringExtra("SORU");
		idquestion=getIntent().getIntExtra("IDQUESTION", 1);
		token=getIntent().getStringExtra("TOKEN");
		sorankisi=getIntent().getStringExtra("SORANKISI");
		date=getIntent().getStringExtra("DATE");
		
		ltCevaplar=(ListView) findViewById(R.id.lvCevaplar);
		
		//TODO Sorular aliniyor..............
		ListeyiHazirla();
				
				
				//Soru gonder
				final EditText etCevap=(EditText) findViewById(R.id.etCevap);
				ImageButton ibSendSoru=(ImageButton) findViewById(R.id.ibSendCevap);
				ibSendSoru.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO cevabin kaydedildigi kisim.
						Utils.saveAnswer(idquestion, etCevap.getText().toString(), token);
						ListeyiHazirla();
						etCevap.setText("");
					}
				});
		
	}
	
	public void ListeyiHazirla(){
		
		try{
			answers=Utils.getAnswers(idquestion);
			}catch(Exception e){
	        	System.out.println(e.getMessage());
	        	Toast.makeText(getApplicationContext(), "Cevaplar alınamadı", Toast.LENGTH_SHORT).show();
	        }
			
			ltCevap=new ArrayList<String>();
				
			ltCevap.add("Soru: "+soru);
			
			for (AnswerVO answerVO : answers) {
				ltCevap.add(answerVO.getDescription());
			}
			
					
					ListeyiDoldur();
		
	}
	
	//Liste metodlari
		public void ListeyiDoldur()
	    {
	      
			 CustomList adapter = new
		                CustomList(CevaplarActivity.this, ltCevap);
		                ltCevaplar.setAdapter(adapter);
		                ltCevaplar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		                    @Override
		                    public void onItemClick(AdapterView<?> parent, View view,
		                                            int position, long id) {
		                    	if(position!=0)
		                        Toast.makeText(CevaplarActivity.this,answers.get(position-1).getDate()+"  tarihinde "+answers.get(position-1).getEmail()+"  tarafından cevaplandı.", Toast.LENGTH_LONG).show();
		                    	else
		                    		 Toast.makeText(CevaplarActivity.this,date+"  tarihinde "+sorankisi+"  tarafindan soruldu.", Toast.LENGTH_LONG).show();
		                    	
		                    }
		                });
			
	    }

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cevaplar, menu);
		return true;
	}*/

}
