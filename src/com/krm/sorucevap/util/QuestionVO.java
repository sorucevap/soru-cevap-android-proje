package com.krm.sorucevap.util;

import java.util.Date;

public class QuestionVO
{
	private Integer idquestion;
	private String email;
	private String category;
	private String title;
	private String description;
	private Date date;
	
	public QuestionVO( Integer idquestion, String email, String category, String title, String description, Date date )
	{
		this.idquestion = idquestion;
		this.email = email;
		this.category = category;
		this.title = title;
		this.description = description;
		this.date = date;
	}
	
	public Integer getIdquestion()
	{
		return idquestion;
	}
	
	public void setIdquestion( Integer idquestion)
	{
		this.idquestion = idquestion;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail( String email)
	{
		this.email = email;
	}
	
	public String getCategory()
	{
		return category;
	}
	
	public void setCategory( String category)
	{
		this.category = category;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle( String title)
	{
		this.title = title;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription( String description)
	{
		this.description = description;
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate( Date date)
	{
		this.date = date;
	}
}
