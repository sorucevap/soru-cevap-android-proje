package com.krm.sorucevap.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.krm.sorucevap.CustomHttpClient;
import com.google.gson.reflect.TypeToken;

public class Utils {
	
	private static final String URL = "http://192.168.2.61:8888/sorucevap/";
	
	private static final String LOGIN = URL + "auth";
	private static final String GETCATEGORIES = URL + "getcategories";
	private static final String GETQUESTIONS = URL + "getquestions";
	private static final String GETANSWERS = URL + "getanswers";
	private static final String LOGOUT = URL + "logout";
	private static final String SAVEANSWER = URL + "saveanswer";
	private static final String SAVEQUESTION = URL + "savequestion";
	private static final String SAVECATEGORY = URL + "savecategory";
	
	public static String authenticate( String email, String pwd, int idgroup)  
	{
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "email", email ) );
		postParameters.add( new BasicNameValuePair( "pwd", pwd ) );
		postParameters.add( new BasicNameValuePair( "idgroup", idgroup+"" ) );
		String result =  CustomHttpClient.executeHttpPost( LOGIN, postParameters );
		if(!result.startsWith( "Exception: " ))return result;
		return null;
	}
	
	public static void logout( String token)
	{
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "token", token ) );
		CustomHttpClient.executeHttpPost( LOGOUT, postParameters );
	}
	
	public static List<QuestionVO> getQuestions(Integer idcategory){    
		List<QuestionVO> list=new ArrayList<QuestionVO>();
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "idcategory", idcategory+"" ) );
		String result =  CustomHttpClient.executeHttpPost( GETQUESTIONS, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException(result);
		
		Type collectionType = new TypeToken<Collection<QuestionVO>>() {}.getType();
		list = Jsonutil.fromJsonToList( result, collectionType );
		return list;
	}

	public static List<AnswerVO> getAnswers(Integer idquestion){      
		List<AnswerVO> list=new ArrayList<AnswerVO>();
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "idquestion", idquestion+"" ) );
		String result =  CustomHttpClient.executeHttpPost( GETANSWERS, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException(result);
		
		Type collectionType = new TypeToken<Collection<AnswerVO>>() {}.getType();
		 list = Jsonutil.fromJsonToList( result, collectionType );
		return list;
	}
	

	public static List<CategoryVO> getCategories(){ 
		List<CategoryVO> list = new ArrayList<CategoryVO>();
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		String result =  CustomHttpClient.executeHttpPost( GETCATEGORIES, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException(result);
		Type collectionType = new TypeToken<Collection<CategoryVO>>() {}.getType();
		list = Jsonutil.fromJsonToList( result, collectionType );
		return list;
	}
	
	public static void saveCategory(String category,String token){    
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "name", category ) );
		postParameters.add( new BasicNameValuePair( "token", token) );
		String result =  CustomHttpClient.executeHttpPost(SAVECATEGORY, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException();
	}

	public static void saveAnswer(Integer idquestion,String answer,String token){    
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "idquestion", idquestion+"" ) );
		postParameters.add( new BasicNameValuePair( "answer", answer ) );
		postParameters.add( new BasicNameValuePair( "token", token) );
		String result =  CustomHttpClient.executeHttpPost(SAVEANSWER, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException();
	}

	public static void saveQuestion(Integer idcategory, String question,String token){    
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add( new BasicNameValuePair( "idcategory", idcategory+"" ) );
		postParameters.add( new BasicNameValuePair( "question", question ) );
		postParameters.add( new BasicNameValuePair( "token", token) );
		String result =  CustomHttpClient.executeHttpPost(SAVEQUESTION, postParameters );
		if(result.startsWith( "Exception: " )) throw new RuntimeException();
	}
	
	
}
