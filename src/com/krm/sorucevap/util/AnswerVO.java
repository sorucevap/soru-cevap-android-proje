package com.krm.sorucevap.util;

import java.util.Date;

public class AnswerVO
{
	private Integer idanswer;
	private String email;
	private int idquestion;
	private String description;
	private Date date;
	
	public AnswerVO( Integer idanswer, String email, int idquestion, String description, Date date )
	{
		this.idanswer = idanswer;
		this.email = email;
		this.idquestion = idquestion;
		this.description = description;
		this.date = date;
	}
	
	public Integer getIdanswer()
	{
		return idanswer;
	}
	
	public void setIdanswer( Integer idanswer)
	{
		this.idanswer = idanswer;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail( String email)
	{
		this.email = email;
	}
	
	public int getIdquestion()
	{
		return idquestion;
	}
	
	public void setIdquestion( int idquestion)
	{
		this.idquestion = idquestion;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription( String description)
	{
		this.description = description;
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate( Date date)
	{
		this.date = date;
	}
}
