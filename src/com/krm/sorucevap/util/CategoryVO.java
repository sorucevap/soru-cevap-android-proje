package com.krm.sorucevap.util;

public class CategoryVO
{
private Integer idcategory;
private String name;
public CategoryVO( Integer idcategory, String name )
{
this.idcategory = idcategory;
this.name = name;
}
public Integer getIdcategory()
{
return idcategory;
}
public void setIdcategory( Integer idcategory)
{
this.idcategory = idcategory;
}
public String getName()
{
return name;
}
public void setName( String name)
{
this.name = name;
}
}