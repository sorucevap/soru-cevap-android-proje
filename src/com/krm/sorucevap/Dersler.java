package com.krm.sorucevap;

public class Dersler {

    public static final String[] arrayDersler = {
    	    "Bilgisayar Mühendisliğine Giriş",
    	    "Bilgisayarın Temelleri",
    	    "Programlamaya Giriş",
    	    "Sayısal Elektronik Laboratuarı",
    	    "Veri Yapıları",
    	    "Mikroişlemciler",
    	    "Algoritmalar",
    	    "Mesleki İngilizce-I",
    	    "Sayısal Tasarım Laboratuarı",
    	    "Programlama Dilleri",
    	   "Mikroişlemciler Laboratuarı",
    	    "Mesleki İngilizce-II",
    	    "Bilgisayar Mimarisi",
    	    "Bilgisayar Sistemleri Laboratuarı",
    	    "Otomata Teorisi",
    	    "Sistem Programlama",
    	    "Yazılım Mühendisliği",
    	    "Bilgisayar Grafikleri-I",
    	    "Web Programlama",
    	    "Bilgisayar Grafikleri Laboratuarı",
    	    "Bilgisayar Grafikleri",
    	    "Tasarım Projesi",
    	    "Windows Programlama",
    	    "Bitirme Çalışması",
    	    "Bilgisayar Ağları Lab."

    };

}