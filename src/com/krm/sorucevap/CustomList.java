package com.krm.sorucevap;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String>{
	
	private final Activity context;
	private final List<String> sorular;
	
	public CustomList(Activity context, List<String> ltSoru) {
		
	super(context, R.layout.sorularlist_row, ltSoru);
	this.context = context;
	this.sorular = ltSoru;
	
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		
	LayoutInflater inflater = context.getLayoutInflater();
	View rowView= inflater.inflate(R.layout.sorularlist_row, null, true);
	TextView txtTitle = (TextView) rowView.findViewById(R.id.tvSoru);
	txtTitle.setText(sorular.get(position).toString());
	return rowView;
	
	}

}
