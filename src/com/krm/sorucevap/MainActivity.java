package com.krm.sorucevap;


import android.os.Bundle;
import android.app.Activity;
import java.util.ArrayList;
import java.util.List;

import com.krm.sorucevap.util.CategoryVO;
import com.krm.sorucevap.util.QuestionVO;
import com.krm.sorucevap.util.Utils;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private int GIRISEKRANI=1;
	private int CEVAPLAREKRANI=2;
	private int SETTINGSSCREEN=3;
	
	SpinnerItemAdapter spinnerAdapter;
	SparseArray<SpinnerItem> spinnerItems;
	Spinner spinner;
	private static final int MAX_TITLE_LENGTH = 50;
	private static final int MAX_DROP_DOWN_TITLE_LENGTH = 50;
	private int spinnerSelectedPosition=-1;
	
	
	private ListView ltSorular;
	private List<String> ltSoru;
	private TextView tvSoru;
	
	List<CategoryVO> categories=new ArrayList<CategoryVO>();
	List<QuestionVO> questions=new ArrayList<QuestionVO>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ltSorular=(ListView) findViewById(R.id.lvSorular);
        
        if(!isSavesPreferences("Email") || !isSavesPreferences("Token")){
    		Intent mainIntent=new Intent(getApplicationContext(), LoginActivity.class);
    		startActivityForResult(mainIntent, GIRISEKRANI);
    		}
        
        
     // SPINNER DERSLER....
      
        spinner = (Spinner) findViewById(R.id.spDersler);
        KategorileriHazirla();
		//END OF SPINNER....
		
		
		//TODO Sorular aliniyor..............
		try{
		questions=Utils.getQuestions((int)-1);
		}catch(Exception e){
			System.out.println("QUESTIONS");
        	System.out.println(e.getMessage());
        	Toast.makeText(getApplicationContext(), "Sorular alınamadı", Toast.LENGTH_SHORT).show();
        }
		ltSoru=new ArrayList<String>();
		
		for (QuestionVO questionVO : questions) {
			ltSoru.add(questionVO.getTitle().toString());
		}
		
		ListeyiDoldur();
		
		
		//Soru gonder
		final EditText etSoru=(EditText) findViewById(R.id.etSoru);
		ImageButton ibSendSoru=(ImageButton) findViewById(R.id.ibSendSoru);
		ibSendSoru.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO soru olusturdugumuz kisim.
				
				if(spinnerSelectedPosition!=-1)
					Utils.saveQuestion(spinnerSelectedPosition, etSoru.getText().toString(), showPreferences("Token"));
				SorulariHazirla();
				etSoru.setText("");
			}
		});
		
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
   
    
    @Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
    	switch (item.getItemId()) {

	      case R.id.action_settings:
	    	  Intent settingIntent=new Intent(getApplicationContext(), SettingsActivity.class);
	    	  settingIntent.putExtra("TOKEN", showPreferences("Token"));
	    	  startActivityForResult(settingIntent, SETTINGSSCREEN);
	        return true; 
	      case R.id.action_cikis:
	    	  Utils.logout(showPreferences("Token"));
	    	  deletePreferences("Email");
			  deletePreferences("Token");
	          finish();
	        return true; 
	      default:  
              return super.onOptionsItemSelected(item);  
	      }
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if (requestCode == GIRISEKRANI) {

		     if(resultCode == RESULT_OK){ 
		         SavePreferences("Email", data.getStringExtra("Email"));
		         SavePreferences("Token", data.getStringExtra("Token"));
		     }
		     if (resultCode == RESULT_CANCELED) {    
		        finish();
		     }
		  }
		 else if(requestCode == CEVAPLAREKRANI){
			 
			 SorulariHazirla();
		 }
         else if(requestCode == SETTINGSSCREEN){
			 
        	 KategorileriHazirla();
		 }
	}
	
	public void SorulariHazirla(){
		try{
			questions=Utils.getQuestions(spinnerSelectedPosition);
			}catch(Exception e){
				System.out.println("QUESTIONS");
	        	System.out.println(e.getMessage());
	        	Toast.makeText(getApplicationContext(), "Sorular alınamadı", Toast.LENGTH_SHORT).show();
	        }
			
			ltSoru=new ArrayList<String>();
			
			for (QuestionVO questionVO : questions) {
				ltSoru.add(questionVO.getDescription().toString());
			}
			
			ListeyiDoldur();
	}
	
	public void KategorileriHazirla(){
		
	    try{
	        categories=Utils.getCategories();     //Dersler burda alindi...
	        }catch(Exception e){
	        	System.out.println("CATAGORIES");
	        	System.out.println(e.getMessage());
	        	Toast.makeText(getApplicationContext(), "Dersler alınamadı", Toast.LENGTH_SHORT).show();
	        }
	        
	    	
			spinnerItems = new SparseArray<SpinnerItem>();
			
			SpinnerItem spinnerItem = new SpinnerItem();
			spinnerItem.title = "...";
			spinnerItem.id = -1;
			spinnerItem.titleLength = MAX_TITLE_LENGTH;
			spinnerItem.titleDropDownLength = MAX_DROP_DOWN_TITLE_LENGTH;
			spinnerItems.append(0, spinnerItem);
			
			for (int i = 1; i < categories.size()+1; i++) {  //TODO spinner icerigi dolduruluyor............
				spinnerItem = new SpinnerItem();
				spinnerItem.title = categories.get(i-1).getName();
				spinnerItem.id = categories.get(i-1).getIdcategory();
				spinnerItem.titleLength = MAX_TITLE_LENGTH;
				spinnerItem.titleDropDownLength = MAX_DROP_DOWN_TITLE_LENGTH;
				spinnerItems.append(i, spinnerItem);
			}
			
			spinnerAdapter = new SpinnerItemAdapter(spinnerItems);	
		
			spinner.setAdapter(spinnerAdapter);
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int pos, long id) {
					
					SpinnerItem spinnerItem = (SpinnerItem) parent
							.getItemAtPosition(pos);
					
					spinnerSelectedPosition=(int) spinnerItem.id;
					
					try{
					questions=Utils.getQuestions((int)spinnerItem.id);
					}catch(Exception e){
						System.out.println("QUESTIONS");
			        	System.out.println(e.getMessage());
			        	Toast.makeText(getApplicationContext(), "Sorular alınamadı", Toast.LENGTH_SHORT).show();
			        }
					
					ltSoru=new ArrayList<String>();
					
					for (QuestionVO questionVO : questions) {
						ltSoru.add(questionVO.getDescription().toString());
					}
					
					ListeyiDoldur();
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
	}
	
	//Liste metodlari
	public void ListeyiDoldur()
    {
      
		 CustomList adapter = new
	                CustomList(MainActivity.this, ltSoru);
	                ltSorular.setAdapter(adapter);
	                ltSorular.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	                    @Override
	                    public void onItemClick(AdapterView<?> parent, View view,
	                                            int position, long id) {
	                    	
	                    
	                        Intent cevaplarIntent=new Intent(getApplicationContext(), CevaplarActivity.class);
	                        cevaplarIntent.putExtra("SORU", questions.get(position).getDescription().toString());
	                        cevaplarIntent.putExtra("SORANKISI", questions.get(position).getEmail().toString());
	                        cevaplarIntent.putExtra("DATE", questions.get(position).getDate().toString());
	                        cevaplarIntent.putExtra("IDQUESTION", questions.get(position).getIdquestion());
	                        cevaplarIntent.putExtra("TOKEN", showPreferences("Token"));
	                        startActivityForResult(cevaplarIntent, CEVAPLAREKRANI);
	                    
	                    }
	                });
		
    }
	

    
    
	//SharedPreferences Metodlari
	
    private void SavePreferences(String key, String value) {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
 
 private boolean isSavesPreferences(String key){
	 SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
	 if(sharedPreferences.contains(key))
		 return true;
	 return false;
 }

    private void deletePreferences(String key) {

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }
   
    private void clearAllPreferences() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
    private String showPreferences(String key){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
       }
    
    public class SpinnerItemAdapter implements SpinnerAdapter {
		
		SparseArray<SpinnerItem> spinnerItem;

		public SpinnerItemAdapter(SparseArray<SpinnerItem> spinnerItem) {
			this.spinnerItem = spinnerItem;
		}

		/**
		 * Returns the Size
		 */
		@Override
		public int getCount() {
			return spinnerItem.size();
		}

		/**
		 * Returns a SpinnerItem Object at the specified position.
		 */
		@Override
		public Object getItem(int position) {
			return spinnerItem.valueAt(position);
		}

		@Override
		public long getItemId(int position) {
			return spinnerItem.valueAt(position).id;
		}

		@Override
		public int getItemViewType(int position) {
			return android.R.layout.simple_spinner_dropdown_item;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			
//			View view = getLayoutInflater().inflate(
//					R.layout.drop_down_view, null);
//			
//			TextView textView = (TextView) view.findViewById(android.R.id.text1);
//			textView.setText(spinnerItem.valueAt(position).getDropDownTitle());
//			return textView;
			
			LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.spinner_item, parent, false);
            TextView spinnerText = (TextView) row.findViewById(R.id.text1);
                spinnerText.setText(spinnerItem.valueAt(position).getDropDownTitle());
            return row;
		
		}
		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view = getLayoutInflater().inflate(
					android.R.layout.simple_spinner_item, null);
			
			TextView textView = (TextView) view.findViewById(android.R.id.text1);
			textView.setText(spinnerItem.valueAt(position).getShortTitle());
			textView.setTextColor(Color.WHITE);
			return textView;
			
		}
		
		
		
	}
    
    class SpinnerItem {

		// SpinnerItem fields, including variable of type SpinnerItem
		public String title = "";
		// sets the width of the spinnerItem
		public int titleLength = 10;// default value
		public int titleDropDownLength = 20;

		public long id;

		// SpinnerItem methods()

		/**
		 * Title with max characters displayed, set by titleLength;
		 * 
		 * @return title of the spinnerItem.
		 */
		public CharSequence getShortTitle() {
			if (title.length() == 0)
				return "?";//
			else if (title.length() > 0 && title.length() <= titleLength) {
				return title;
			} else {
				String shortTile = title.substring(0, titleLength).trim()
						+ "..";
				return shortTile;
			}
		}

		public CharSequence getDropDownTitle() {
			if (title.length() == 0)
				return "?";//
			else if (title.length() > 0
					&& title.length() <= titleDropDownLength) {
				return title;
			} else {
				String shortTile = title.substring(0, titleDropDownLength)
						.trim() + "..";
				return shortTile;
			}
		}
	}
    
}
