package com.krm.sorucevap;

import java.io.BufferedReader;
import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class CustomHttpClient
{
	
	private static HttpClient getHttpClient()
	{
		try {
			KeyStore trustStore = KeyStore.getInstance( KeyStore.getDefaultType() );
			trustStore.load( null, null );
			
			SSLSocketFactory sf = new MySSLSocketFactory( trustStore );
			sf.setHostnameVerifier( SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER );
			
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion( params, HttpVersion.HTTP_1_1 );
			HttpProtocolParams.setContentCharset( params, HTTP.UTF_8 );
			
			SchemeRegistry registry = new SchemeRegistry();
			registry.register( new Scheme( "http", PlainSocketFactory.getSocketFactory(), 80 ) );
			registry.register( new Scheme( "https", sf, 443 ) );
			
			ClientConnectionManager ccm = new ThreadSafeClientConnManager( params, registry );
			
			return new DefaultHttpClient( ccm, params );
		} catch ( Exception e ) {
			return new DefaultHttpClient();
		}
	}
	
	/**
	 * Performs an HTTP Post request to the specified url with the specified parameters.
	 * 
	 * @param url
	 *            The web address to post the request to
	 * @param postParameters
	 *            The parameters to send via the request
	 * @return The result of the request
	 * @throws Exception
	 */
	public static String executeHttpPost( String url, ArrayList<NameValuePair> postParameters)
	{
		BufferedReader in = null;
		try {
			HttpClient client = getHttpClient();
			HttpPost request = new HttpPost( url );
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity( postParameters, "UTF-8" );
			request.setEntity( formEntity );
			HttpResponse response = client.execute( request );
			return EntityUtils.toString( response.getEntity() );
		} catch ( Exception e ) {
			e.printStackTrace();
			throw new RuntimeException( e );
		} finally {
			if ( in != null ) {
				try {
					in.close();
				} catch ( IOException e ) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String httppost( String link, String xml)
	{
		HttpClient httpclient = getHttpClient();
		
		try {
			HttpPost httppost = new HttpPost( link );
			httppost.setHeader( "Content-type", "text/xml; charset=UTF-8" );
			httppost.setEntity( new StringEntity( xml, "UTF-8" ) );
			
			HttpResponse response = httpclient.execute( httppost );
			int status = response.getStatusLine().getStatusCode();
			if ( status != HttpStatus.SC_OK ) {
				throw new RuntimeException( link + " httppost failed... - " + response.getStatusLine() );
			}
			HttpEntity entity = response.getEntity();
			if ( entity != null ) {
				return EntityUtils.toString( entity );
			}
			return null;
		} catch ( IOException ex ) {
			if ( ex instanceof HttpHostConnectException ) {
				Log.w( "tag",
				        "Server'a restart atiliyor. Su anda cevap veremez! link: " + link + " EX: " + ex.getMessage()
				                + " xml: " + xml );
			} else {
				Log.e( "tag", "HttpPost IO EXCEPTION xml: " + xml + " EX: ", ex );
			}
			throw new RuntimeException( ex );
		} catch ( RuntimeException ex ) {
			Log.e( "tag", "HttpPost RUNTIME EXCEPTION xml: " + xml, ex );
			throw ex;
		} finally {
			httpclient.getConnectionManager().shutdown();
			
		}
	}
}
