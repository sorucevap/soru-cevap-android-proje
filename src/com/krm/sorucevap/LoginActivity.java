package com.krm.sorucevap;


import com.krm.sorucevap.util.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	
	private int secim=1;
	private String token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		final RadioButton rbAkademisyen=(RadioButton) findViewById(R.id.rbAkademisyen);
		final RadioButton rbOgrenci=(RadioButton) findViewById(R.id.rbOgrenci);
        Button btGiris=(Button) findViewById(R.id.btGiris);
		
		final EditText etEMail=(EditText) findViewById(R.id.etAdSoyad);
		final EditText etSifre=(EditText) findViewById(R.id.etNumara);
		etEMail.setText("");
		etSifre.setText("");
		TextView tv1=(TextView) findViewById(R.id.textView1);
		TextView tv2=(TextView) findViewById(R.id.textView2);
		
		
	
		
		rbAkademisyen.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					rbOgrenci.setChecked(false);
				secim=2;
				
			}
		});
		
rbOgrenci.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					rbAkademisyen.setChecked(false);
				secim=1;
			}
		});

btGiris.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		
		token=Utils.authenticate(etEMail.getText().toString(), etSifre.getText().toString(), secim);
		
		if(token!=null){
			
			Intent returnIntent = new Intent();
			 returnIntent.putExtra("Email",etEMail.getText().toString());
			 returnIntent.putExtra("Token",token);
			 setResult(RESULT_OK,returnIntent);  
				finish();
			
		}
		else{
			Toast.makeText(getApplicationContext(), "E-mail veya şifre yanlış.Tekrar deneyiniz.", Toast.LENGTH_SHORT).show();
			etEMail.setText("");
			etSifre.setText("");
		}
		
	}
});
		
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent returnIntent = new Intent();
			 setResult(RESULT_CANCELED,returnIntent);  
				finish();

			}

			return super.onKeyDown(keyCode, event);
	}

	

}
